#!/bin/sh

DIR=`echo $0|sed 's/\/getfiles.sh//'`

cd $DIR

# Get previous max inserted ID
MAXID=`echo 'select max(newest_id) from latest;' | ./dbish 2>/dev/null | gawk 'NR==4 { print $2 }'`

echo Fetching 02packages ...

(
  wget --no-check-certificate -q -O 02packages http://cpan.metacpan.org/modules/02packages.details.txt.gz &&
  mv 02packages 02packages.details.txt.gz
) ||
  exit

echo Fetching CPAN-testers database ...

# this talks to the CPAN-testers metabase and populates our
# cpantesters db
./refill-cpanstatsdb.pl --finishlimit=1 --quiet
echo refill finished at `date +%H:%M:%S`

# move/rewrite records into cpandeps[dev] db
./mangledb.pl
echo mangle finished at `date +%H:%M:%S`

./populate-cache.pl
echo populate finished at `date +%H:%M:%S`
./build-reverse-index.pl
echo build-reverse finished at `date +%H:%M:%S`

echo
echo
echo "List of Unknown OSes follows (may be empty)"
echo
echo "select platform, origosname, count(*) from cpanstats where id > $MAXID and os='Unknown OS' and platform <> '' and origosname <> '' group by platform, origosname;" |./dbish 2>/dev/null
