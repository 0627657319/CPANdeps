use strict;
use warnings;
use Test::More;
use lib './lib';
use CPANdepsUtils;

my @tests = (
	{
		file     => 'A/AB/ABALAMA/App-MonM-1.07.tar.gz',
		author   => 'ABALAMA',
		dist     => 'App-MonM-1.07',
		distname => 'App-MonM',
		version  => '1.07',
	},
	{
		file     => 'M/MB/MBETHKE/Lingua-LO-NLP-v1.0.3.tar.bz2',
		author   => 'MBETHKE',
		dist     => 'Lingua-LO-NLP-v1.0.3',
		distname => 'Lingua-LO-NLP',
		version  => 'v1.0.3',
	},
	{
		file     => 'J/JB/JBRYAN/AI-NeuralNet-BackProp-0.89.zip',
		author   => 'JBRYAN',
		dist     => 'AI-NeuralNet-BackProp-0.89',
		distname => 'AI-NeuralNet-BackProp',
		version  => '0.89',
	},
	{
		file     => 'S/SM/SMCMURRAY/Apache-AxKit-Provider-CGI-0.02.tgz',
		author   => 'SMCMURRAY',
		dist     => 'Apache-AxKit-Provider-CGI-0.02',
		distname => 'Apache-AxKit-Provider-CGI',
		version  => '0.02',
	},
	{
		file     =>
		'S/SC/SCHWIGON/perl-formance/Benchmark-Perl-Formance-0.53.tar.gz',
		author   => 'SCHWIGON',
		dist     => 'Benchmark-Perl-Formance-0.53',
		distname => 'Benchmark-Perl-Formance',
		version  => '0.53',
	},
);

plan tests => 4 * @tests;

for my $t (@tests) {
	my $res = CPANdepsUtils::parse_distfile ($t->{file});
	is $res->{$_}, $t->{$_}, "$_ is $t->{$_}" for
		qw/author dist distname version/;
}
