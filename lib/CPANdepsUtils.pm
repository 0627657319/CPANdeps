package CPANdepsUtils;

use strict;
use warnings;

use IPC::ConcurrencyLimit;

sub concurrency_limit {
    my $lockfile = shift;
    my $max_procs = shift || 1;
    my $type_of_request = shift || "text";

    my $limit = IPC::ConcurrencyLimit->new(
        max_procs => $max_procs,
        path      => $lockfile,
    );
    my $limitid = $limit->get_lock;
    if(not $limitid) {
        if($type_of_request eq 'text') {
            warn "Another process appears to be still running. Exiting.";
        } elsif($type_of_request eq 'html') {
            print '<meta http-equiv="refresh" content="15">Sorry, we\'re really busy right now, please wait for a bit. This page will automagically refresh soon'
        } elsif($type_of_request eq 'xml') {
            print "<?xml version='1.0'?><cpandeps><error>Sorry, too busy, try again later</error></cpandeps>";
        }
        exit(0);
    }
    return $limit;
}

=head2 parse_distfile

Given a full package path as stored in the C<file> column of the packages
table, returns a hashref with these fields:

=over

=item author

=item dist (the name and version with joining character)

=item distname (just the name of the dist, no version)

=item version (just the version of the dist)

=cut

sub parse_distfile {
	my $distfile = shift;
	return unless defined $distfile;

	if ($distfile =~
		m#^./../([^/]+)(?:/[/\w-]+)?/(([^/]+)[-_](v?\d+[\d_.a-z]*))\.(?:tar\.(?:gz|bz2)|tgz|zip)#
	  ) {
		return {
			author   => $1,
			dist     => $2,
			distname => $3,
			version  => ($4 // 0),
		};
	}
	return;
}

1;
