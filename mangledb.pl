#!/usr/local/bin/perl

use strict;
use warnings;
use DBI;
use Data::Dumper;
use FindBin;
use Time::Piece;
use Getopt::Long;
use version 'is_lax';

chdir($FindBin::Bin);
my $dbname = ($FindBin::Bin =~ /dev/) ? 'cpandepsdev' : 'cpandeps';

use lib "$FindBin::Bin/lib";
use CPANdepsUtils;
my $limit = CPANdepsUtils::concurrency_limit("/tmp/$dbname/refill-deps-db/lock");
my $DEBUG = 0;
my $timings = 0;
GetOptions (
	'v|verbose' => \$DEBUG,
	't|timings' => \$timings,
);

$| = 1;

my @dbauth = ($ENV{DB_USER}, $ENV{DB_PASS});
my $cpandepsdbh    = DBI->connect("dbi:mysql:database=$dbname", @dbauth);
my $cpantestersdbh = DBI->connect("dbi:mysql:database=cpantesters", @dbauth);
print "$0: Starting at " . localtime->hms . "\n" if $timings;

if (-e '02packages.details.txt.gz') {
  print "Putting 02packages into db ...\n";
  my $sth = $cpandepsdbh->prepare("INSERT INTO packages (module, version, file) VALUES (?, ?, ?)");
  $cpandepsdbh->{'AutoCommit'} = 0;
  $cpandepsdbh->do('DELETE FROM packages');
  open(PACKAGES, 'gzip -dc 02packages.details.txt.gz |');
      while(<PACKAGES> ne "\n") {}; # throw away headers
      while(my $line = <PACKAGES>) {
          chomp($line);
          my($module, $version, $file) = split(/\s+/, $line, 3);
  	die("Couldn't import [$module, $version, $file]\n")
  	  unless($sth->execute($module, $version, $file));
      }
  close(PACKAGES);
  unlink('02packages.details.txt.gz');
  $cpandepsdbh->{'AutoCommit'} = 1;
  print "$0: Packages table rebuilt at " . localtime->hms . "\n" if $timings;
} else {
  print "No new packages to import\n";
}

my %latest;
my $outputstep = 10000;
print "Finding/inserting new test results.  Each dot is $outputstep records ...\n";
{
  my @os_by_osname = (
    '' => 'Unknown OS',
    'aix' => 'AIX',
    'android' => 'Android',
    'beos' => 'BeOS',
    'bitrig' => 'Bitrig BSD',
    'bsdos' => 'BSD OS',
    'cygwin' => 'Windows (Cygwin)',
    'darwin' => 'Mac OS X',
    'Mac OS X' => 'Mac OS X',
    'dec_osf' => 'Tru64/OSF/Digital UNIX',
    'dragonfly' => 'Dragonfly BSD',
    'freebsd' => 'FreeBSD',
    'gnu' => 'GNU Hurd',
    'gnukfreebsd' => 'FreeBSD (Debian)',
    'haiku' => 'Haiku',
    'hpux' => 'HP-UX',
    'interix' => 'Interix (MS services for Unix)',
    'irix' => 'Irix',
    'linux' => 'Linux',
    'GNU/Linux' => 'Linux',
    'linThis'   => 'Linux',
    'linuxThis' => 'Linux',
    'lThis'     => 'Linux',
    'linuThis'  => 'Linux',
    'macos' => 'Mac OS classic',
    'midnightbsd' => 'Midnight BSD',
    'minix'       => 'Minix',
    'mirbsd' => 'MirOS BSD',
    'mswin32' => 'Windows (Win32)',
    'netbsd' => 'NetBSD',
    'openbsd' => 'OpenBSD',
    'openosname=openbsd' => 'OpenBSD',
    'openThis' => 'OpenBSD',
    'os2' => 'OS/2',
    'os390' => 'OS390/zOS',
    'nto' => 'QNX Neutrino',
    'sco' => 'SCO Unix',
    'solaris' => 'Solaris',
    'vms' => 'VMS',
  );
  my $insert = $cpandepsdbh->prepare('
    INSERT INTO cpanstats (id, state, dist, version, perl, is_dev_perl, os, platform, origosname)
      VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)
  ');

  # can't deal with eleventy zillion records at once, so eat them this many at a time
  my $records_to_fetch = 1000000;

SELECTLOOP:
  my $insertcount = 0;
  my $maxid = $cpandepsdbh->selectall_arrayref('SELECT MAX(id) FROM cpanstats')->[0]->[0] || 0;
  my $select = $cpantestersdbh->prepare("
    SELECT id, state, dist, version, platform, perl, platform, osname
      FROM cpanstats
     WHERE id > $maxid AND
           id < $maxid + $records_to_fetch AND
           state IN ('pass', 'fail', 'na', 'unknown') AND
	   perl != '0'
  ");
  $select->execute();
  $cpandepsdbh->{'AutoCommit'} = 0;
  while(my $record = $select->fetchrow_hashref()) {
    # NB the order of these two lines is important!
    $record->{is_dev_perl} = ($record->{perl} =~ /(^v?5\.(7|9|[1-9][13579]))|rc|patch/i) ? 1 : 0;
    $record->{perl} =~ s/\s+(RC|patch).*//i;
    $record->{perl} =~ s/^v//;
    $record->{os} = 'Unknown OS';
    my @temp_os_by_osname = @os_by_osname;
    while(@temp_os_by_osname) {
      my($osname, $os) = (shift(@temp_os_by_osname), shift(@temp_os_by_osname));
      if($record->{osname} && $record->{osname} =~ /^$osname$/i) {
        $record->{os} = $os;
        last;
      }
    }
    if($record->{os} eq 'Unknown OS') { # if we couldn't map it try looking at 'platform'
        $record->{platform} =~ /linux/ ? $record->{os} = 'Linux'
      : warn(sprintf(
          "Couldn't map osname '%s', platform '%s'\n",
          $record->{osname}, $record->{platform}
      ))
    }

    $insert->execute(
      map { $record->{$_} } qw(id state dist version perl is_dev_perl os platform osname)
    );
    if(!(++$insertcount % $outputstep)) {
      print '.';
      $cpandepsdbh->{'AutoCommit'} = 1;
      $cpandepsdbh->{'AutoCommit'} = 0;
    }
	my $ldvpo = $latest{$record->{dist}       }
	                   {$record->{version}    }
					   {$record->{perl}       }
					   {$record->{is_dev_perl}}
					   {$record->{os}         } //= {};
	$ldvpo->{$record->{state}}++;
	$ldvpo->{latest} = $record->{id} unless
		defined $ldvpo->{latest} &&
		$ldvpo->{latest} > $record->{id};
  }
  $cpandepsdbh->{'AutoCommit'} = 1;

  # times 0.9 because there are occasional gaps in the series, especially
  # early on
  goto SELECTLOOP if($insertcount > $records_to_fetch * 0.9);

  print "\n";
  print "Test results finished at " . localtime->hms . "\n" if $timings;

  unless ($insertcount) {
    print "No new tests found/inserted\n";
    # Consider exiting here
  }
}

# Now feed into latest
$cpandepsdbh->{'AutoCommit'} = 0;
my $lusth = $cpandepsdbh->prepare ('UPDATE latest SET pass = pass + ?, fail =
fail + ?, unknown = unknown + ?, na = na + ?, newest_id = ?
WHERE dist_id = ? AND version = ? AND perl = ? AND is_dev_perl = ? AND os = ?');
my $listh = $cpandepsdbh->prepare ('INSERT INTO latest
(pass, fail, unknown, na, newest_id, dist_id, version, perl, is_dev_perl, os)
VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
my $didsth = $cpandepsdbh->prepare ('SELECT id FROM dists WHERE dist = ?');
my $ndsth = $cpandepsdbh->prepare ('INSERT INTO dists VALUES (NULL, ?)');
my $svsth = $cpandepsdbh->prepare ('SELECT version FROM latest WHERE dist_id = ?');
my $dovsth = $cpandepsdbh->prepare ('DELETE FROM latest WHERE dist_id = ? AND version = ?');
my %totals = (
	insertions       => 0,
	updates          => 0,
	deleted_rows     => 0,
	deleted_versions => 0
);

for my $dist (keys %latest) {
	my $new_dist = 0;

	# Get dist ID
	$didsth->execute ($dist);
	my ($dist_id) = $didsth->fetchrow_array;
	unless (defined $dist_id) {
		print "New dist: $dist\n" if $DEBUG;
		my $res = $ndsth->execute ($dist);
		unless ($res && $res == 1) {
			warn "Bad res ($res) inserting $dist: skipping";
			next;
		}
		($dist_id) = $cpandepsdbh->selectrow_array ('SELECT LAST_INSERT_ID()');
		$new_dist = 1;
	}

	# Get existing versions if dist is old
	my $oldver = 0;
	unless ($new_dist) {
		$svsth->execute ($dist_id);
		($oldver) = $svsth->fetchrow_array;
		unless (defined $oldver) {
			print "No tests found for previous version of $dist\n";
			$oldver = 0;
		}
	}

	# Sort the versions
	my @newvers = keys %{$latest{$dist}};
	if (1 < @newvers) {
		# Clean, sort, newest first
		print "Sorting version @newvers " if $DEBUG;
		@newvers = sort_versions (@newvers);
		print " into (@newvers)\n" if $DEBUG;
	}

	# Delete the old versions, if any
	if ($oldver && $newvers[0] ne $oldver) {
		my $res = $dovsth->execute ($dist_id, $oldver);
		unless ($res) {
			warn "Bad result deleting old version $oldver " .
				"of $dist: $DBI::errstr";
		} else {
			$totals{deleted_rows} += $res;
			$totals{deleted_versions}++;
		}
	}

	# Update the new results or insert them
	my $thisv = $latest{$dist}{$newvers[0]};
	for my $perl (keys %$thisv) {
		for my $is_dev (keys %{$thisv->{$perl}}) {
			for my $os (keys %{$thisv->{$perl}{$is_dev}}) {
				my @bargs = @{$thisv->{$perl}{$is_dev}{$os}}{qw/pass fail na unknown latest/};
				$_ //= 0 for @bargs;
				push @bargs, $dist_id, $newvers[0], $perl, $is_dev, $os;
				# Only attempt the update if dist is not new and version is old.
				unless ($new_dist || $newvers[0] ne $oldver) {
					my $res = $lusth->execute (@bargs);
					unless ($res) {
						warn "Update failed for $dist_id, $newvers[0], " .
							"$perl, $is_dev, $os with latest " .
							"$thisv->{$perl}{$is_dev}{$os}{latest}\n";
					}
					$totals{updates} += $res;
					next if $res && $res == 1;
				}
				my $res = $listh->execute (@bargs);
				$totals{insertions} += $res;
				warn "Bad res $res inserting (@bargs): $DBI::errstr" unless
					$res && $res == 1;
			}
		}
	}
}
$cpandepsdbh->{'AutoCommit'} = 1; # Commit

print "latest table stats:
	inserted rows    = $totals{insertions}
	updated rows     = $totals{updates}
	deleted rows     = $totals{deleted_rows}
	deleted versions = $totals{deleted_versions}\n";

$cpandepsdbh->do ('OPTIMIZE LOCAL TABLE latest')
	if $totals{updates} + $totals{deleted_rows};

mkdir 'db';
chmod 0777, 'db';

sub sort_versions {
	my @v = @_;
	unless (grep { ! is_lax ($_) } @v) {
		return sort { version->parse ($b) <=> version->parse ($a) } @v;
	}

	# Schwarzian Transform
	return
		map { $_->[0] }
		sort { $b->[1] <=> $a->[1] }
		map {
			my $c = $_;
			$c =~ s/[a-z_-]/0/ig;
			$c = version->parse ($c);
			$c = version->parse (0.00) if "$c" eq 'v.Inf'; # Handle really bad ones
			[$_, $c] }
		@v;
}
