#!/usr/local/bin/perl
#===============================================================================
#
#         FILE: build-reverse-index.pl
#
#        USAGE: ./build-reverse-index.pl
#
#  DESCRIPTION: Build reverse dep lists
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#        NOTES: Rewritten for speed - now completes in approx 10 mins
#         BUGS: The reading of the YAML/JSON files is still slow
#       AUTHOR: Pete Houston (pete), cpan@openstrike.co.uk
# ORGANIZATION: Openstrike
#      VERSION: 1.01
#      CREATED: 27/06/21 13:24:36
#===============================================================================

use strict;
use warnings;

use DBI;
use Data::Dumper;
use FindBin;
use YAML::XS ();
use JSON::MaybeXS;
use Time::Piece;
use List::MoreUtils 'uniq';
use Ref::Util 'is_arrayref';
use Getopt::Long;

$/ = undef;

my $timings = 0;               # Output timing info at each step
my $path    = 'db/reverse';    # Use 'db/reverse' for prod
GetOptions (
	't|timings' => \$timings,
);

chdir $FindBin::Bin;
my $dbname = ($FindBin::Bin =~ /dev/) ? 'cpandepsdev' : 'cpandeps';

use lib "$FindBin::Bin/lib";
use CPANdepsUtils;
my $limit =
  CPANdepsUtils::concurrency_limit ("/tmp/$dbname/rebuild-reverse-index/lock");

print "Building reverse index ...\n";
print "$0: Starting at " . localtime->hms . "\n" if $timings;

mkdir 'db';
mkdir $path;
chmod 0777, 'db', $path;

my %rev;
my $srcpath = 'db/META.yml';
my $i = 0;
my %seen;
opendir my $dirh, $srcpath or die "can't open $srcpath\n";
foreach (grep { -s "$srcpath/$_" && -f "$srcpath/$_" } readdir ($dirh)) {
	my ($fname) = /(.*)\.(?:yml|json)$/;
	next if $seen{$fname};
	$i++;
	my @deps;
	eval {
		if (/yml$/) {
			my $this = YAML::XS::LoadFile ("$srcpath/$_");
			@deps = (
				keys %{$this->{requires}},
				keys %{$this->{build_requires}},
				keys %{$this->{configure_requires}},
				keys %{$this->{test_requires}}
			);
		} else {
			open my $metah, '<', "$srcpath/$_" or die "Can't read $srcpath/$_\n";
			my $this = decode_json (<$metah>);
			close $metah;
			@deps = (
				keys %{$this->{prereqs}->{runtime}->{requires}},
				keys %{$this->{prereqs}->{configure}->{requires}},
				keys %{$this->{prereqs}->{build}->{requires}},
				keys %{$this->{prereqs}->{test}->{requires}},
			);
		}
	};
	if (!$@ && @deps) {
		$seen{$fname} = 1;

		# Clean up the filename so we just have the distname
		s/\.(json|yml)$//;
		s/-v?\d[\d.]*$//;

		# The keys of %rev are the modules.
		for my $dep (uniq @deps) {
			if (exists $rev{$dep}) {
				push @{$rev{$dep}}, $_;
			} else {
				$rev{$dep} = [$_];
			}
		}
	}
}
closedir $dirh;
%seen = ();

print "$0: Master hash built from $i files at " . localtime->hms . "\n" if $timings;

my $dbh =
  DBI->connect ("dbi:mysql:database=$dbname", $ENV{DB_USER}, $ENV{DB_PASS});

my $sth = $dbh->prepare (
	'SELECT file, module FROM packages
	ORDER BY file, module'
);
$sth->execute;

my %files;
while (my ($file, $mod) = $sth->fetchrow_array) {
	if (exists $files{$file}) {
		push @{$files{$file}}, $mod;
	} else {
		$files{$file} = [$mod];
	}
}

$dbh->disconnect;

print "$0: Files and mods listed at " . localtime->hms . "\n" if $timings;

my %rdep;
foreach my $file (sort keys %files) {
    my $distinfo = CPANdepsUtils::parse_distfile ($file) or next;
    my($author, $dist, $ver) = ($distinfo->{author}, $distinfo->{distname},
		$distinfo->{version});
	next if (!defined ($author) || !defined ($dist));

	my @modules = sort { $a cmp $b } uniq
	  map { is_arrayref ($rev{$_}) ? @{$rev{$_}} : () } @{$files{$file}};
	if (!exists $rdep{$dist} || ($rdep{$dist}->{ver} cmp $ver) < 0) {
	    $rdep{$dist} = { ver => $ver, rdeps => \@modules };
	}
}

# Dump huge rdep hash to filestore
open my $outf, '>', "$path/full.json" or die "Can't write $path/full.json\n";
print $outf encode_json (\%rdep);
close $outf;

print "$0: Finished at " . localtime->hms . "\n" if $timings;
